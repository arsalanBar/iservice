'use strict';
(function () {
  let hamburger = document.querySelector('.menu-hamburger');
  let menu = document.querySelector('.menu');

  function hamburgerClick() {
    hamburger.classList.toggle('open');
    menu.classList.toggle('open');
    document.body.classList.toggle('overflow-hidden');
  }
  hamburger.addEventListener('click', hamburgerClick);

  let menuItems = document.querySelectorAll('.menu__nested');
  let backBtn = document.querySelector('.menu__back');

  backBtn.addEventListener('click', function () {
    menuItems.forEach(function (item) {
      item.classList.remove('open');
    });
    backBtn.classList.remove('show');
  });

  let menuClick = function () {
    if (event.type === "click") {
      let item = event.target.parentElement;
      if (item.classList.contains('open')) {
        return true;
      } else {
        item.classList.add('open');
        backBtn.classList.add('show');
        return false;
      }
    }
  };
  if (document.documentElement.clientWidth <= 768) {
    menuItems.forEach(function (item) {
      item.addEventListener('click', menuClick);
    });
  } else {
    menuItems.forEach(function (item) {
      item.removeEventListener('click', menuClick);
    });
  }
  window.addEventListener('resize', function () {
    menuItems.forEach(function (item) {
      item.removeEventListener('click', menuClick());
    });
    if (document.documentElement.clientWidth <= 768) {
      menuItems.forEach(function (item) {
        item.addEventListener('click', menuClick);
      });
    } else {
      menuItems.forEach(function (item) {
        item.removeEventListener('click', menuClick);
      });
    }
  });
})();
