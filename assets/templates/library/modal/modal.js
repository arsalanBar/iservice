'use strict';

let Modal = function (modal, callerSelector, onModalOpen) {
  this.modal = modal;
  this.callers = document.querySelectorAll(callerSelector);
  this.callers.forEach(function (caller) {
    caller.addEventListener('click', function () {
      event.preventDefault();
      modal.classList.add('modal__open');
      document.body.classList.toggle('overflow-hidden');
      onModalOpen();
    });
  });
  let modalClose = this.modal.querySelector('.modal__close');
  modalClose.addEventListener('click', modalCloseClick);

  function modalCloseClick() {
    modal.classList.toggle('modal__open');
    document.body.classList.toggle('overflow-hidden');
  }
};
Modal.prototype.openModal = function () {
  this.modal.classList.add('modal__open');
};
Modal.prototype.closeModal = function () {
  let modalClose = this.modal.querySelector('.modal__close');
  modalClose.addEventListener('click', modalCloseClick);

  function modalCloseClick() {
    this.modal.classList.toggle('modal__open');
  }
};

function createModals(jsonModals) {
  jsonModals.forEach(function (modal) {
    let modalObj = document.querySelector(modal.obj);
    let modal1 = new Modal(modalObj, modal.caller, modal.onModalOpen);
  });
}
