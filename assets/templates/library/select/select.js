'use strict';
// let placeholderData = 'Select an option';
let preloadData = [{
    'text': 'Group 1',
    'children': [{
        'id': 1,
        'text': 'Option 1.1'
      },
      {
        'id': 2,
        'text': 'Option 1.2'
      }
    ]
  },
  {
    'text': 'Group 2',
    'children': [{
        'id': 3,
        'text': 'Option 2.1'
      },
      {
        'id': 4,
        'text': 'Option 2.2'
      }
    ]
  }
];

$(document).ready(function () {
  $('.select').each(function (index, element) {
    $(element).select2({
      placeholder: $(element).data('placeholder'),
      data: preloadData, // удалить при ajax запросах
      width: 'style'
      // ajax: {
      //   url: 'https://',
      //   dataType: 'json'
      //   // остальные параметры ajax
      // }
    });
  });
});
