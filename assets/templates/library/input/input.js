var inputs = document.querySelectorAll('.input');

var inputsDisabled = document.querySelectorAll('.input.disabled');

inputsDisabled.forEach(function (input) {
  input.setAttribute('disabled', 'disabled');
});

var inputClick = function (q, event) {};

var keyCodes = [];

for (let i = 48; i < 91; i++) {
  keyCodes.push(i);
}

keyCodes.push(96);
keyCodes.push(97);
keyCodes.push(98);
keyCodes.push(99);
keyCodes.push(100);
keyCodes.push(101);
keyCodes.push(102);
keyCodes.push(103);
keyCodes.push(104);
keyCodes.push(105);
keyCodes.push(106);
keyCodes.push(107);
keyCodes.push(108);
keyCodes.push(109);
keyCodes.push(110);
keyCodes.push(111);
keyCodes.push(186);
keyCodes.push(187);
keyCodes.push(188);
keyCodes.push(189);
keyCodes.push(190);
keyCodes.push(191);
keyCodes.push(192);
keyCodes.push(219);
keyCodes.push(220);
keyCodes.push(221);
keyCodes.push(222);
keyCodes.push(32);

inputs.forEach(function (input, i) {
  if (input.getAttribute('data-focuslen')) {
    input.addEventListener('keydown', function (event) {
      const keyCode = event.keyCode;
      console.log(event.keyCode);
      console.log(keyCodes);
      console.log(keyCodes.indexOf(keyCode));
      if (input.value.length >= input.getAttribute('data-focuslen') && (keyCodes.indexOf(keyCode) >= 0)) {
        document.querySelectorAll('.input')[i + 1].focus();
      }
    });
  }
  if (!input.getAttribute('data-focusEnter')) {
    input.addEventListener('keydown', function (event) {
      const keyName = event.key;
      if (event.key === 'Enter') {
        document.querySelectorAll('.input')[i + 1].focus();
      }
    });
  }

});
