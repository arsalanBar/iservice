'use strict';
var myRevSwiper = new Swiper('.reviews__content .reviews__wrapper', {
  slidesPerView: 2,
  spaceBetween: 20,
  speed: 200,
  // Navigation arrows
  navigation: {
    nextEl: '.swiper-reviews-slider_next',
    prevEl: '.swiper-reviews-slider_prev',
  },
  breakpoints: {
    // when window width is <= 480px
    480: {
      slidesPerView: 1,
      spaceBetween: 10
    }
  }
});

let reviewModal = document.querySelector('.review-modal .reviews__wrapper');
let fullReview = document.querySelector('.reviews-all');
if (fullReview) {
  let fullReviewClone = fullReview.cloneNode(true);
  fullReviewClone.querySelectorAll('.review__footer').forEach(function (item) {
    let modalLink = fullReviewClone.querySelector('.review__link');
    item.removeChild(modalLink);
  });

  fullReviewClone.classList.remove('block__content', 'page-reviews__content');

  fullReviewClone.classList.add('reviews__swiper', 'swiper-wrapper');
  reviewModal.appendChild(fullReviewClone);
}


var myModalSwiper = new Swiper('.review-modal .reviews__wrapper', {
  slidesPerView: 1,
  spaceBetween: 0,
  speed: 200,
  navigation: {
    nextEl: '.modal-reviews-slider_next',
    prevEl: '.modal-reviews-slider_prev',
  }
});

(function () {

  let links = document.querySelectorAll('.review__link');
  if (links) {
    links.forEach(function (link, index) {
      link.dataset.id = index;
      link.addEventListener('mouseup', function () {
        myModalSwiper.slideTo(event.target.dataset.id);
      });
    });
  }

})();
