'use strict';
(function () {

  // console.log('test page-add-review');
  function onDocumentScroll() {
    if (window.pageYOffset >= 500) {
      buttonUp.classList.add('show');
    } else {
      buttonUp.classList.remove('show');
    }
  }

  function onButtonUpClick() {
    document.body.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'start'
    });
  }
  let buttonUp = document.querySelector('.up-btn');
  if (buttonUp) {
    document.addEventListener('scroll', onDocumentScroll);
    buttonUp.addEventListener('click', onButtonUpClick);
  }

})();
