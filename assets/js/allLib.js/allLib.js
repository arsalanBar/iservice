'use strict';
(function () {

  // console.log('test page-add-review');
  function onDocumentScroll() {
    if (window.pageYOffset >= 500) {
      buttonUp.classList.add('show');
    } else {
      buttonUp.classList.remove('show');
    }
  }

  function onButtonUpClick() {
    document.body.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'start'
    });
  }
  let buttonUp = document.querySelector('.up-btn');
  if (buttonUp) {
    document.addEventListener('scroll', onDocumentScroll);
    buttonUp.addEventListener('click', onButtonUpClick);
  }

})();











var inputs = document.querySelectorAll('.input');

var inputsDisabled = document.querySelectorAll('.input.disabled');

inputsDisabled.forEach(function (input) {
  input.setAttribute('disabled', 'disabled');
});

var inputClick = function (q, event) {};

var keyCodes = [];

for (let i = 48; i < 91; i++) {
  keyCodes.push(i);
}

keyCodes.push(96);
keyCodes.push(97);
keyCodes.push(98);
keyCodes.push(99);
keyCodes.push(100);
keyCodes.push(101);
keyCodes.push(102);
keyCodes.push(103);
keyCodes.push(104);
keyCodes.push(105);
keyCodes.push(106);
keyCodes.push(107);
keyCodes.push(108);
keyCodes.push(109);
keyCodes.push(110);
keyCodes.push(111);
keyCodes.push(186);
keyCodes.push(187);
keyCodes.push(188);
keyCodes.push(189);
keyCodes.push(190);
keyCodes.push(191);
keyCodes.push(192);
keyCodes.push(219);
keyCodes.push(220);
keyCodes.push(221);
keyCodes.push(222);
keyCodes.push(32);

inputs.forEach(function (input, i) {
  if (input.getAttribute('data-focuslen')) {
    input.addEventListener('keydown', function (event) {
      const keyCode = event.keyCode;
      console.log(event.keyCode);
      console.log(keyCodes);
      console.log(keyCodes.indexOf(keyCode));
      if (input.value.length >= input.getAttribute('data-focuslen') && (keyCodes.indexOf(keyCode) >= 0)) {
        document.querySelectorAll('.input')[i + 1].focus();
      }
    });
  }
  if (!input.getAttribute('data-focusEnter')) {
    input.addEventListener('keydown', function (event) {
      const keyName = event.key;
      if (event.key === 'Enter') {
        document.querySelectorAll('.input')[i + 1].focus();
      }
    });
  }

});



'use strict';
(function () {
  let hamburger = document.querySelector('.menu-hamburger');
  let menu = document.querySelector('.menu');

  function hamburgerClick() {
    hamburger.classList.toggle('open');
    menu.classList.toggle('open');
    document.body.classList.toggle('overflow-hidden');
  }
  hamburger.addEventListener('click', hamburgerClick);

  let menuItems = document.querySelectorAll('.menu__nested');
  let backBtn = document.querySelector('.menu__back');

  backBtn.addEventListener('click', function () {
    menuItems.forEach(function (item) {
      item.classList.remove('open');
    });
    backBtn.classList.remove('show');
  });

  let menuClick = function () {
    if (event.type === "click") {
      let item = event.target.parentElement;
      if (item.classList.contains('open')) {
        return true;
      } else {
        item.classList.add('open');
        backBtn.classList.add('show');
        return false;
      }
    }
  };
  if (document.documentElement.clientWidth <= 768) {
    menuItems.forEach(function (item) {
      item.addEventListener('click', menuClick);
    });
  } else {
    menuItems.forEach(function (item) {
      item.removeEventListener('click', menuClick);
    });
  }
  window.addEventListener('resize', function () {
    menuItems.forEach(function (item) {
      item.removeEventListener('click', menuClick());
    });
    if (document.documentElement.clientWidth <= 768) {
      menuItems.forEach(function (item) {
        item.addEventListener('click', menuClick);
      });
    } else {
      menuItems.forEach(function (item) {
        item.removeEventListener('click', menuClick);
      });
    }
  });
})();

'use strict';

let Modal = function (modal, callerSelector, onModalOpen) {
  this.modal = modal;
  this.callers = document.querySelectorAll(callerSelector);
  this.callers.forEach(function (caller) {
    caller.addEventListener('click', function () {
      event.preventDefault();
      modal.classList.add('modal__open');
      document.body.classList.toggle('overflow-hidden');
      onModalOpen();
    });
  });
  let modalClose = this.modal.querySelector('.modal__close');
  modalClose.addEventListener('click', modalCloseClick);

  function modalCloseClick() {
    modal.classList.toggle('modal__open');
    document.body.classList.toggle('overflow-hidden');
  }
};
Modal.prototype.openModal = function () {
  this.modal.classList.add('modal__open');
};
Modal.prototype.closeModal = function () {
  let modalClose = this.modal.querySelector('.modal__close');
  modalClose.addEventListener('click', modalCloseClick);

  function modalCloseClick() {
    this.modal.classList.toggle('modal__open');
  }
};

function createModals(jsonModals) {
  jsonModals.forEach(function (modal) {
    let modalObj = document.querySelector(modal.obj);
    let modal1 = new Modal(modalObj, modal.caller, modal.onModalOpen);
  });
}







'use strict';
var myRevSwiper = new Swiper('.reviews__content .reviews__wrapper', {
  slidesPerView: 2,
  spaceBetween: 20,
  speed: 200,
  // Navigation arrows
  navigation: {
    nextEl: '.swiper-reviews-slider_next',
    prevEl: '.swiper-reviews-slider_prev',
  },
  breakpoints: {
    // when window width is <= 480px
    480: {
      slidesPerView: 1,
      spaceBetween: 10
    }
  }
});

let reviewModal = document.querySelector('.review-modal .reviews__wrapper');
let fullReview = document.querySelector('.reviews-all');
if (fullReview) {
  let fullReviewClone = fullReview.cloneNode(true);
  fullReviewClone.querySelectorAll('.review__footer').forEach(function (item) {
    let modalLink = fullReviewClone.querySelector('.review__link');
    item.removeChild(modalLink);
  });

  fullReviewClone.classList.remove('block__content', 'page-reviews__content');

  fullReviewClone.classList.add('reviews__swiper', 'swiper-wrapper');
  reviewModal.appendChild(fullReviewClone);
}


var myModalSwiper = new Swiper('.review-modal .reviews__wrapper', {
  slidesPerView: 1,
  spaceBetween: 0,
  speed: 200,
  navigation: {
    nextEl: '.modal-reviews-slider_next',
    prevEl: '.modal-reviews-slider_prev',
  }
});

(function () {

  let links = document.querySelectorAll('.review__link');
  if (links) {
    links.forEach(function (link, index) {
      link.dataset.id = index;
      link.addEventListener('mouseup', function () {
        myModalSwiper.slideTo(event.target.dataset.id);
      });
    });
  }

})();

'use strict';
// let placeholderData = 'Select an option';
let preloadData = [{
    'text': 'Group 1',
    'children': [{
        'id': 1,
        'text': 'Option 1.1'
      },
      {
        'id': 2,
        'text': 'Option 1.2'
      }
    ]
  },
  {
    'text': 'Group 2',
    'children': [{
        'id': 3,
        'text': 'Option 2.1'
      },
      {
        'id': 4,
        'text': 'Option 2.2'
      }
    ]
  }
];

$(document).ready(function () {
  $('.select').each(function (index, element) {
    $(element).select2({
      placeholder: $(element).data('placeholder'),
      data: preloadData, // удалить при ajax запросах
      width: 'style'
      // ajax: {
      //   url: 'https://',
      //   dataType: 'json'
      //   // остальные параметры ajax
      // }
    });
  });
});







//# sourceMappingURL=allLib.js.map
