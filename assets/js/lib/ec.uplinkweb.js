var easyComm = {
    initialize: function () {
        if (!jQuery().ajaxForm) {
            easyComm.notice.error('Can`t find jQuery ajaxForm plugin!');
        }
        easyComm.rating.initialize();
        jQuery(document).on('submit', 'form.ec-form', function (e) {
            easyComm.message.send(this);
            e.preventDefault();
            return false;
        });
    },

    message: {
        send: function (form) {
            jQuery(form).ajaxSubmit({
                data: {action: 'message/create'},
                url: easyCommConfig.actionUrl,
                form: form,
                dataType: 'json',
                beforeSubmit: function () {
                    jQuery(form).find('.reviews-form__btn').attr('disabled', 'disabled');
                    jQuery(form).find('.has-error').removeClass('has-error');
                    jQuery(form).find('.ec-error').text('').hide();
                    return true;
                },
                success: function (response) {
                    var fid = jQuery(form).data('fid');
                    jQuery(form).find('.reviews-form__btn').removeAttr('disabled');
                    if (response.success) {
                        jQuery(form)[0].reset();
                        if (typeof (response.data) == "string") {
                            jQuery('#ec-form-success-' + fid).html(response.data);                           
                            jQuery(form).removeClass('reviews-form--active');
                            jQuery('.reviews__show-form').text('Написать отзыв');
                            var removeAlert = function () {
                                jQuery('#ec-form-success-' + fid).html('');
                            };
                            setTimeout(removeAlert, 3000);
                        }
                        else {
                            easyComm.notice.show(response.message);
                        }
                    }
                    else {
                        if (response.data && response.data.length) {
                            jQuery.each(response.data, function (i, error) {
                                jQuery(form).find('[name="' + error.field + '"]').addClass('has-error');
                                jQuery(form).find('#ec-' + error.field + '-error-' + fid).text(error.message).show();
                            });
                        } else {
                            easyComm.notice.error(response.message);
                        }
                    }
                }
                , error: function () {
                    jQuery(form).find('.reviews-form__btn').removeAttr('disabled');
                    easyComm.notice.error('Submit error');
                }
            });
        }
    },


    rating: {
        initialize: function () {
            var stars = jQuery('.ec-rating').find('.rating-star');
            stars.on('touchend click', function (e) {
                jQuery(this).parent().children().removeClass('active');
                jQuery(this).prevAll().addClass('active');
                jQuery(this).addClass('active');
                // save vote
                var storageId = jQuery(this).closest('.ec-rating').data('storage-id');
                jQuery('#' + storageId).val(jQuery(this).data('rating'));
            });
            stars.hover(
                // hover in
                function () {
                    jQuery(this).addClass('active2');
                    jQuery(this).prevAll().addClass('active2');
                    jQuery(this).nextAll().removeClass('active2');
                },
                // hover out
                function () {
                    jQuery(this).parent().children().removeClass('active2');
                }
            );
        }
    },

    notice: {
        error: function (text) {
            alert(text);
        },
        show: function (text) {
            alert(text);
        }
    }
}

jQuery(document).ready(function () {
    easyComm.initialize();
});

var easyCommReCaptchaCallback = function () {
    if (typeof grecaptcha !== 'undefined') {
        $('.ec-captcha').each(function (index) {
            grecaptcha.render($(this).attr('id'), {
                'sitekey': easyCommConfig.reCaptchaSiteKey
            });
        });
    }
    else {
        easyComm.notice.error('grecaptcha is not defined!');
    }
}